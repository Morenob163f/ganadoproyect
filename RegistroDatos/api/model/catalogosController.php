<?php          
    include_once('./utils/odbcclient.php');
    class categoria 
    {                        
        public function getPerfiles() 
        {                   
            global $ConnectionString, $output;
            $ds = null;
            try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();
                    $cmd = $conn->createCommand();
                    $cmd->setCommandText("dbo.obtenerPerfiles");
                    $ds = $cmd->executeDataSet();
                    $conn->close();
            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }
            $categorias = array();
          
            foreach($ds as $row) {
                    $newRow = array();
                    $newRow["id"] = $row["id"];
                    $newRow["nombre"] = utf8_encode($row["nombre"]);
                    array_push($categorias, $newRow);
            }        
            echo json_encode($categorias);
        }
    }
    
?>