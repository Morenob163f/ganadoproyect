<?php
include_once('./utils/odbcclient.php');

class Ganado
{
    public function Guardar($data)
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            /*   foreach($data as $row) {             
                $codigo =  $row[0]["codigo"];                        
                $nombre = $row[0]["nombre"];                        
                $categoria = $row[0]["categoria"];                        
                $descripcion = $row[0]["descripcion"];                        
                }   */
            $idPerfil = $data["idPerfil"];
            $nombre =  $data["nombre"];
            $peso = $data["peso"];
            $costo = $data["costo"];
            $descripcion = $data["descripcion"];

            // $mynewstring = str_replace(" \" ", " ", $stringArticulo, $count);           
            $query = "dbo.GuardarGanado " . "'" . $idPerfil . "'" . "," . "'" . $nombre . "'" . "," . "'" . $peso . "'" . "," . "'" . $costo . "'" . "," . "'" . $descripcion . "'";

            // echo json_encode($query);
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();
                //$conn->beginTransaction(); roll back

                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
            }
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        echo json_encode($ds);
    }
    public function ObtenerGanado()
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            $conn = new OdbcConnection();
            $conn->setConnectionString($ConnectionString['conexiondb']);
            $conn->open();
            $cmd = $conn->createCommand();
            $cmd->setCommandText("dbo.ObtenerGanado");
            $ds = $cmd->executeDataSet();
            $conn->close();
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        $ganado = array();

        foreach ($ds as $row) {
            $newRow = array();
            $newRow["idGanado"] = $row["idGanado"];
            $newRow["nombre"] = $row["nombre"];
            $newRow["peso"] = $row["peso"];
            $newRow["costo"] = $row["costo"];
            $newRow["descripcion"] = $row["descripcion"];
            array_push($ganado, $newRow);
        }
        echo json_encode($ganado);
    }
    public function ObtenerGanadoEnfermo()
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            $conn = new OdbcConnection();
            $conn->setConnectionString($ConnectionString['conexiondb']);
            $conn->open();
            $cmd = $conn->createCommand();
            $cmd->setCommandText("dbo.ObtenerGanadoEnfermo");
            $ds = $cmd->executeDataSet();
            $conn->close();
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        $ganado = array();

        foreach ($ds as $row) {
            $newRow = array();
            $newRow["idGanado"] = $row["idGanado"];
            $newRow["nombre"] = $row["nombre"];
            $newRow["temperatura"] = $row["temperatura"];
            $newRow["Cardiaca"] = $row["Cardiaca"];
            $newRow["Respiratoria"] = $row["Respiratoria"];
            $newRow["Sanguinia"] = $row["Sanguinia"];
            array_push($ganado, $newRow);
        }
        echo json_encode($ganado);
    }
    public function ObtenerGanadoDetalles()
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            $conn = new OdbcConnection();
            $conn->setConnectionString($ConnectionString['conexiondb']);
            $conn->open();
            $cmd = $conn->createCommand();
            $cmd->setCommandText("dbo.ObtenerGanadoDetalles");
            $ds = $cmd->executeDataSet();
            $conn->close();
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        $ganado = array();

        foreach ($ds as $row) {
            $newRow = array();
            $newRow["idGanado"] = $row["idGanado"];
            $newRow["nombre"] = $row["nombre"];
            $newRow["temperatura"] = $row["temperatura"];
            $newRow["Cardiaca"] = $row["Cardiaca"];
            $newRow["Respiratoria"] = $row["Respiratoria"];
            $newRow["Sanguinia"] = $row["Sanguinia"];
            array_push($ganado, $newRow);
        }
        echo json_encode($ganado);
    }
    public function ObtenerDietas()
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            $conn = new OdbcConnection();
            $conn->setConnectionString($ConnectionString['conexiondb']);
            $conn->open();
            $cmd = $conn->createCommand();
            $cmd->setCommandText("dbo.ObtenerDieta");
            $ds = $cmd->executeDataSet();
            $conn->close();
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }

        echo json_encode($ds);
    }
    public function ObtenerClasificacion($data)
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            $idGanado = $data["idGanado"];
            $conn = new OdbcConnection();
            $conn->setConnectionString($ConnectionString['conexiondb']);
            $conn->open();
            $cmd = $conn->createCommand();
            $query = "dbo.ObtenerClasificacion " . "'" . $idGanado . "'";
            $cmd->setCommandText($query);
            $ds = $cmd->executeDataSet();
            $conn->close();
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }

        echo json_encode($ds);
    }
    public function obtenerSacrificios()
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            $conn = new OdbcConnection();
            $conn->setConnectionString($ConnectionString['conexiondb']);
            $conn->open();
            $cmd = $conn->createCommand();
            $cmd->setCommandText("dbo.ObtenerGanadoSacrificio");
            $ds = $cmd->executeDataSet();
            $conn->close();
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }

        echo json_encode($ds);
    }
    public function AsignarDieta($data)
    {
        //echo json_encode($data);       
        global $ConnectionString, $output;
        $ds = null;
        try {
            $idGanado = $data["idGanado"];
            $idDieta = $data["idDieta"];
            $query = "dbo.AsignarDieta " . "'" . $idGanado . "'" . "," . "'" . $idDieta . "'";

            //echo json_encode($query);               
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();
                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
            }
            $detalles = array();
            foreach ($ds as $row) {
                $newRow = array();
                $newRow["bandera"] = $row["bandera"];
                array_push($detalles, $newRow);
            }
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        echo json_encode($detalles);
    }
    public function sacrificar($data)
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            // $idPerfil = $data["idPerfil"];
            $idGanado =  $data["idGanado"];
            $nombre =  $data["nombre"];
            $peso = $data["peso"];
            $costo = $data["costo"];
            $sensor = $data["sensor"];
            $descripcion = $data["descripcion"];


            $query = "dbo.sacrificaGanado " . "'" . $idGanado . "'" . "," . "'" . $nombre . "'" . "," . "'" . $peso . "'" . "," . "'" . $costo . "'" . "," . "'" . $sensor . "'" . "," . "'" . $descripcion . "'";

            //echo json_encode($query);
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();
                //$conn->beginTransaction(); roll back

                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
            }
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        echo json_encode($ds);
    }
    public function capturarClasificacion($data)
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            // $idPerfil = $data["idPerfil"];
            $idGanado =  $data["idGanado"];
            $peso = $data["peso"];
            $color = $data["color"];
            $marmoleo = $data["marmoleo"];
     


            $query = "dbo.capturarClasificacion " . "'" . $idGanado . "'" . "," . "'" . $peso . "'" . "," . "'" . $color . "'" . "," . "'" . $marmoleo . "'";

            //echo json_encode($query);
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();
                //$conn->beginTransaction(); roll back

                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
            }
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        echo json_encode($ds);
    }
    public function ObtenerReporte($data)
    {
        global $ConnectionString, $output;
        $ds = null;
        try {
            // $idPerfil = $data["idPerfil"];
            $fechaini =  $data["fechaini"];
            $fechafin = $data["fechafin"];
       
     


            $query = "dbo.ObtenerReporteGanado " . "'" . $fechaini . "'" . "," . "'" . $fechafin . "'";

            //echo json_encode($query);
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();
                //$conn->beginTransaction(); roll back

                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
            }
        } catch (Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }
        echo json_encode($ds);
    }
    
}
