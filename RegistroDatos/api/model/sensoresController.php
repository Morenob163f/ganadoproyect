<?php          
    include_once('./utils/odbcclient.php');
    class detalle
    {                            
    public function ObtenerDetalles($data) 
    {                   
        global $ConnectionString, $output;
        $ds = null;      
        try {                   
            $idGanado = $data["idGanado"];  
                                                    
                                                  
            // $mynewstring = str_replace(" \" ", " ", $stringArticulo, $count);           
            $query = "dbo.ObtenerDetallesGanado " . "'". $idGanado. "'". "";  

           // echo json_encode($query);
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();
       
                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
        
            }
            $detalles = array();
          
            foreach($ds as $row) {
                    $newRow = array();
                    $newRow["idGanado"] = $row["idGanado"];
                    $newRow["temperatura"] = $row["temperatura"];
                    $newRow["Cardiaca"] = $row["Cardiaca"];
                    $newRow["Respiratoria"] = $row["Respiratoria"];
                    $newRow["Sanguinia"] = $row["Sanguinia"];
                    array_push($detalles, $newRow);
            }        
            echo json_encode($detalles);
        }
        catch(Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }                    
    }
    
    public function EditarDetalleGanado($data) 
    {              
        //echo json_encode($data);       
        global $ConnectionString, $output;
        $ds = null;      
        try {                                                                                                  
            $query = "dbo.EditarDetalleGanado"." " .$data["idGanado"] . ",". "'". $data["temperatura"]. "'". ",". "'".$data["Cardiaca"]. "'". ",".$data["Respiratoria"].",". "'".$data["Sanguinia"]."'";     
            //echo json_encode($query);               
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();          
                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
        
            }
            $detalles = array();
            foreach($ds as $row) {
                $newRow = array();
                $newRow["idGanado"] = $row["idGanado"];
                $newRow["temperatura"] = $row["temperatura"];
                $newRow["Cardiaca"] = $row["Cardiaca"];
                $newRow["Respiratoria"] = $row["Respiratoria"];
                $newRow["Sanguinia"] = $row["Sanguinia"];
                array_push($detalles, $newRow);
        }        

    }
        catch(Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }                    
        echo json_encode($detalles);
    }
    public function AsignarCuarentena($data) 
    {              
        //echo json_encode($data);       
        global $ConnectionString, $output;
        $ds = null;      
        try {         
            $idGanado = $data["idGanado"];                                                                                           
            $query = "dbo.AsignarCuarentena " . "'". $idGanado. "'". ""; 
            //echo json_encode($query);               
            try {
                $conn = new OdbcConnection();
                $conn->setConnectionString($ConnectionString['conexiondb']);
                $conn->open();          
                $cmd = $conn->createCommand();
                $rollback = FALSE;
                $cmd->setCommandText($query);
                $ds = $cmd->executeDataSet();
                $conn->close();
            } catch (Exception $e) {
        
            }
            $detalles = array();
            foreach($ds as $row) {
                $newRow = array();
                $newRow["bandera"] = $row["bandera"];
                array_push($detalles, $newRow);
        }        

    }
        catch(Exception $ex) {
            throw new restfulException(1, "Error interno en el servicio", 500);
        }                    
        echo json_encode($detalles);
    }
    
}
?>