<?php
        class restfulException extends Exception {
                public $http_code;
                
                public function __construct($status, $message, $http_code = 400) {
                        $this->http_code = $http_code;
                        $this->message = $message;
                        $this->code = $status;
                }
        }
